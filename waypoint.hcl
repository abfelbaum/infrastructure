project = "infrastructure"

app "updater" {
  build {
    use "docker-pull" {
      image = "registry.git.abfelbaum.dev/abfelbaum/images/builder"
      tag   = "latest"

      disable_entrypoint = true
    }
  }

  deploy {
    use "nomad-jobspec" {
      jobspec = "${path.app}/${var.nomad_file_path}.nomad"
    }
  }
}

variable "nomad_file_path" {
  type        = string
  env         = ["CI_NOMAD_FILE"]
  description = "The nomad file to deploy"
  sensitive   = false
}