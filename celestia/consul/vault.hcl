service {
  name    = "vault"
  id      = "vault"
  address = "10.1.0.10"
  port    = 8200

  meta {
    external-node  = "true"
    external-probe = "true"
  }

  connect {
    sidecar_service {}
  }

  tags = [
    "traefik.enable=true",
    "traefik.http.routers.vault.rule=Host(`vault.cloudsdale.servers.abfelbaum.dev`)",
    "traefik.http.routers.vault.entrypoints=internal",
    "traefik.http.routers.vault.tls.certresolver=leresolver"
  ]
}