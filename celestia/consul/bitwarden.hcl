service {
  name    = "bitwarden"
  id      = "bitwarden"
  address = "10.1.0.10"
  port    = 8080

  meta {
    external-source = "docker"
  }

  connect {
    sidecar_service {
      proxy {
        config {
          bind_address = "0.0.0.0"
          bind_port    = 21003

          envoy_local_cluster_json = "{\"@type\": \"type.googleapis.com/envoy.config.cluster.v3.Cluster\",\"name\": \"local_app\",\"type\": \"LOGICAL_DNS\",\"connect_timeout\": \"5s\",\"circuit_breakers\": {\"thresholds\": [ {\"priority\": \"DEFAULT\",\"max_connections\": 2048 } ] }, \"load_assignment\": { \"cluster_name\": \"local_app\", \"endpoints\": [ { \"lb_endpoints\": [ { \"endpoint\": { \"address\": { \"socket_address\": { \"address\": \"bitwarden-nginx\", \"port_value\": 8080 } } } } ] } ] } }"
        }
      }
    }
  }

  tags = [
    "traefik.enable=true",
    "traefik.http.routers.bitwarden.rule=Host(`vault.abfelbaum.dev`)",
    "traefik.http.routers.bitwarden.entrypoints=websecure",
    "traefik.http.routers.bitwarden.tls.certresolver=leresolver",
    "traefik.http.routers.bitwarden.tls.options=mintls12@file",
  ]
}