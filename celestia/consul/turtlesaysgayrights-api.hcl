service {
  name    = "turtlesaysgayrightsmc-api"
  id      = "tsgrmc-api"
  address = "10.1.0.100"
  port    = 4567

  meta {
    external-node  = "true"
    external-probe = "true"
  }
  
  connect {
    sidecar_service {}
  }

  tags = [
    "traefik.enable=true",
    "traefik.http.routers.turtlesaysgayrightsmc-api.rule=Host(`api.turtlesaysgayrightsmc.abfelbaum.dev`)",
    "traefik.http.routers.turtlesaysgayrightsmc-api.entrypoints=internal",
    "traefik.http.routers.turtlesaysgayrightsmc-api.tls.certresolver=leresolver"
  ]
}