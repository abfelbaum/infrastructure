service {
  name    = "turtlesaysgayrightsmc-map"
  id      = "tsgrmc-map"
  address = "10.1.0.100"
  port    = 80

  meta {
    external-node  = "true"
    external-probe = "true"
  }

  connect {
    sidecar_service {}
  }

  tags = [
    "traefik.enable=true",
    "traefik.http.routers.turtlesaysgayrightsmc-map.rule=Host(`map.turtlesaysgayrightsmc.abfelbaum.dev`)",
    "traefik.http.routers.turtlesaysgayrightsmc-map.entrypoints=websecure",
    "traefik.http.routers.turtlesaysgayrightsmc-map.tls.certresolver=leresolver"
  ]
}