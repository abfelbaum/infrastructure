service {
  name    = "paperless"
  id      = "paperless"
  address = "10.1.0.100"
  port    = 8000

  meta {
    external-node  = "true"
    external-probe = "true"
  }

  connect {
    sidecar_service {  }
  }

  tags = [
    "traefik.enable=true",
    "traefik.http.routers.paperless.rule=Host(`paperless.internal.abfelbaum.dev`)",
    "traefik.http.routers.paperless.entrypoints=internal",
    "traefik.http.routers.paperless.tls.certresolver=leresolver"
  ]
}