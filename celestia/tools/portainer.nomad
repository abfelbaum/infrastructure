job "portainer" {
  datacenters = ["celestia"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  group "portainer" {
    count = 1
    
    network {
      mode = "bridge"
    }

    service {
      name = "portainer"
      port = 9000

      connect {
        sidecar_service {}
      }
      
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.portainer.rule=Host(`portainer.cloudsdale.servers.abfelbaum.dev`)",
        "traefik.http.routers.portainer.entrypoints=internal",
        "traefik.http.routers.portainer.tls.certresolver=leresolver"
      ]
    }

    task "portainer" {
      driver = "docker"

      config {
        image = "portainer/portainer-ee:2.16.2"

        volumes = [
          "/var/run/docker.sock:/var/run/docker.sock",
          "/opt/portainer/data/:/data/"
        ]
      }
    }
  }
}