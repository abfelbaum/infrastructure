job "zitadel" {
  datacenters = ["celestia"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  group "database" {
    count = 1

    network {
      mode = "bridge"

      port "crdb-http" {
        to = 8080
      }
    }

    service {
      name = "zitadel-crdb"
      port = 26257

      connect {
        sidecar_service {}
      }
    }

    volume "certs" {
      type      = "host"
      source    = "zitadel-crdb-certs"
      read_only = false
    }

    volume "data" {
      type      = "host"
      source    = "zitadel-crdb-data"
      read_only = false
    }

    task "cockroach" {
      driver = "docker"

      config {
        image = "cockroachdb/cockroach:v22.2.7"

        command = "start-single-node"
      }

      volume_mount {
        volume      = "data"
        destination = "/cockroach/cockroach-data"
      }

      volume_mount {
        volume      = "certs"
        destination = "/cockroach/certs"
      }

      service {
        check {
          name     = "zitadel crdb health"
          type     = "http"
          protocol = "http"
          port     = "crdb-http"
          interval = "10s"
          timeout  = "2s"
          #          address_mode = "alloc"
          path     = "/health?ready=1"
        }
      }

      resources {
        cpu    = 500
        memory = 2048
      }
    }
  }

  group "zitadel" {
    count = 1

    network {
      mode = "bridge"

      port "http" {
        to = 8080
      }
    }

    service {
      name = "zitadel"
      port = "http"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "zitadel-crdb"
              local_bind_port  = 26257
            }
          }
        }
      }

      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=false",
        "traefik.http.routers.zitadel.rule=HostRegexp(`auth.abfelbaum.dev`, `{subdomain:[a-z]+}.auth.abfelbaum.dev`)",
        "traefik.http.routers.zitadel.entrypoints=websecure",
        "traefik.http.routers.zitadel.tls.certresolver=leresolver",
        "traefik.http.routers.zitadel.tls.domains[0].main=auth.abfelbaum.dev",
        "traefik.http.routers.zitadel.tls.domains[0].sans=*.auth.abfelbaum.dev",
        "traefik.http.routers.zitadel.middlewares=zitadel",
        "traefik.http.services.zitadel.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
        "traefik.http.services.zitadel.loadbalancer.server.url=h2c://${NOMAD_IP_http}:${NOMAD_HOST_PORT_http}",
        "traefik.http.services.zitadel.loadbalancer.server.scheme=h2c",
        "traefik.http.services.zitadel.loadbalancer.passHostHeader=true",

        "traefik.http.middlewares.zitadel.headers.isDevelopment=false",
        "traefik.http.middlewares.zitadel.headers.allowedHosts=auth.abfelbaum.dev",

      ]
    }

    volume "certs" {
      type      = "host"
      source    = "zitadel-certs"
      read_only = true
    }

    task "zitadel" {
      driver = "docker"

      config {
        image = "ghcr.io/zitadel/zitadel:stable"

        force_pull = true

        command = "start-from-init"

        args = [
          "--masterkeyFromEnv",
          "--tlsMode",
          "external"
        ]

        ports = ["http"]
      }

      volume_mount {
        volume      = "certs"
        destination = "/crdb-certs/"
        read_only   = true
      }

      vault {
        env      = false
        policies = ["zitadel"]
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/environment.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/tools/zitadel" }}
          ZITADEL_MASTERKEY = "{{.Data.data.MASTERKEY}}"

          ZITADEL_DATABASE_COCKROACH_USER_USERNAME = "{{.Data.data.DB_USER}}"
          ZITADEL_DATABASE_COCKROACH_USER_PASSWORD = "{{.Data.data.DB_PASSWORD}}"

          ZITADEL_DATABASE_COCKROACH_USER_SSL_ROOTCERT = "{{.Data.data.CA_CERT}}"
          ZITADEL_DATABASE_COCKROACH_USER_SSL_CERT = "{{.Data.data.USER_CERT}}"
          ZITADEL_DATABASE_COCKROACH_USER_SSL_KEY = "{{.Data.data.USER_CERT_KEY}}"

          ZITADEL_DATABASE_COCKROACH_ADMIN_USERNAME = "{{.Data.data.DB_ADMIN_USER}}"
          ZITADEL_DATABASE_COCKROACH_ADMIN_PASSWORD = "{{.Data.data.DB_ADMIN_PASSWORD}}"

          ZITADEL_DATABASE_COCKROACH_ADMIN_SSL_ROOTCERT = "{{.Data.data.CA_CERT}}"
          ZITADEL_DATABASE_COCKROACH_ADMIN_SSL_CERT = "{{.Data.data.ADMIN_CERT}}"
          ZITADEL_DATABASE_COCKROACH_ADMIN_SSL_KEY = "{{.Data.data.ADMIN_CERT_KEY}}"
{{ end }}
        EOF
      }

      env {
        ZITADEL_LOG_LEVEL                         = "info"
        ZITADEL_EXTERNALSECURE                    = true
        ZITADEL_EXTERNALPORT                      = 443
        ZITADEL_EXTERNALDOMAIN                    = "auth.abfelbaum.dev"
        ZITADEL_DATABASE_COCKROACH_HOST           = "${NOMAD_UPSTREAM_IP_zitadel_crdb}"
        ZITADEL_DATABASE_COCKROACH_USER_SSL_MODE  = "verify-full"
        ZITADEL_DATABASE_COCKROACH_ADMIN_SSL_MODE = "verify-full"
      }

      resources {
        cpu    = 500
        memory = 600
      }
    }
  }
}
