job "fff-gmb" {
  datacenters = ["luna"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  group "database" {
    network {
      mode = "bridge"
    }

    service {
      name = "fff-gmb-mariadb"
      port = 3306

      connect {
        sidecar_service {}
      }
    }

    volume "data" {
      type            = "csi"
      source          = "fff-gmb"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    task "mariadb" {
      driver = "docker"

      config {
        image = "centos/mariadb-102-centos7"
      }

      volume_mount {
        volume      = "data"
        destination = "/var/lib/mysql/"
      }

      vault {
        env      = false
        policies = ["fff-gmb-mariadb", "fff-gmb"]
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/database.root.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/bots/fff-gmb/mariadb" }}
          MYSQL_ROOT_PASSWORD={{.Data.data.MYSQL_ROOT_PASSWORD}}
{{ end }}
        EOF
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/database.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/bots/fff-gmb" }}
          MYSQL_USER={{.Data.data.DB_USERNAME}}
          MYSQL_PASSWORD={{.Data.data.DB_PASSWORD}}
          MYSQL_DATABASE={{.Data.data.DB_DATABASE}}
{{ end }}
        EOF
      }
    }
  }

  group "bot" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "fff-gmb"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "fff-gmb-mariadb"
              local_bind_port  = 3306
            }
          }
        }
      }
    }

    task "fff-gmb" {
      driver = "docker"

      config {
        image = "registry.git.abfelbaum.dev/edwardsnowden/groupmanagementbot:v6.3.2"
      }

      env {
        PYTHONUNBUFFERED = 1

        MYSQL_DB_HOST    = "${NOMAD_UPSTREAM_IP_fff_gmb_mariadb}"
        MYSQL_DB_PORT    = "${NOMAD_UPSTREAM_PORT_fff_gmb_mariadb}"
        PLUGIN_BLACKLIST = "network_thevillage"
        
        NETIQUETTE_LINK  = "https://pad.fridaysforfuture.de/p/FFF-Chats_Netiquette/export/html"
      }

      vault {
        env      = false
        policies = ["fff-gmb"]
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/environment.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/bots/fff-gmb" }}
          TOKEN={{.Data.data.BOT_TOKEN}}
          MYSQL_DB_USERNAME={{.Data.data.DB_USERNAME}}
          MYSQL_DB_PASSWORD={{.Data.data.DB_PASSWORD}}
          MYSQL_DB_DATABASE={{.Data.data.DB_DATABASE}}
          MASTER_ADMIN_ID={{.Data.data.MASTER_ADMIN_ID}}
{{ end }}
        EOF
      }
    }
  }
}