job "peoplespolicyprojectblogbot" {
  datacenters = ["luna"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  group "bot" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "peoplespolicyprojectblogbot"
    }

    volume "database" {
      type            = "csi"
      source          = "peoplespolicyprojectblogbot"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    task "peoplespolicyprojectblogbot" {
      driver = "docker"

      config {
        image = "registry.git.abfelbaum.dev/edwardsnowden/mattbruenigblogbot"
      }

      volume_mount {
        volume      = "database"
        destination = "/bot/volumes/"
      }

      env {
        PYTHONUNBUFFERED = 1
      }

      vault {
        env      = false
        policies = ["peoplespolicyprojectblogbot"]
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/environment.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/bots/peoplespolicyprojectblogbot" }}
          BOT_TOKEN={{.Data.data.BOT_TOKEN}}
          CHANNEL_ID={{.Data.data.CHANNEL_ID}}
{{ end }}
        EOF
      }
    }
  }
}