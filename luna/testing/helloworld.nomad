job "hello-world" {
  datacenters = ["luna"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  group "hello-world" {
    count = 2

    network {
      mode = "bridge"
    }

    service {
      name = "hello-world"
      port = 80

      connect {
        sidecar_service {}
      }

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.hello.rule=Host(`hello.cloudsdale.servers.abfelbaum.dev`)",
        "traefik.http.routers.hello.entrypoints=websecure",
        "traefik.http.routers.hello.tls.certresolver=leresolver",
      ]
    }

    task "hello-world" {
      driver = "docker"

      config {
        image = "strm/helloworld-http"
      }
    }
  }
}