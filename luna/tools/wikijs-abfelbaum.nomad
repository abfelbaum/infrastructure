job "wikijs-abfelbaum" {
  datacenters = ["luna"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  group "database" {
    count = 1

    network {
      mode = "bridge"
    }


    service {
      name = "wikijs-abfelbaum-pgsql"
      port = 5432

      connect {
        sidecar_service {}
      }
    }

    volume "data" {
      type            = "csi"
      source          = "wikijs-abfelbaum-pgsql"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    task "postgres" {
      driver = "docker"

      config {
        image = "postgres:11-alpine"
      }

      volume_mount {
        volume      = "data"
        destination = "/var/lib/postgresql/data/"
      }

      vault {
        env      = false
        policies = ["wiki-abfelbaum"]
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/environment.database.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/tools/wiki-abfelbaum" }}
          POSTGRES_USER={{.Data.data.DB_USER}}
          POSTGRES_PASSWORD={{.Data.data.DB_PASSWORD}}
          POSTGRES_DB={{.Data.data.DB_DATABASE}}
{{ end }}
        EOF
      }
    }
  }

  group "wikijs" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "wikijs-abfelbaum"
      port = 3000

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "wikijs-abfelbaum-pgsql"
              local_bind_port  = 5432
            }
          }
        }
      }

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.wiki-abfelbaum.rule=Host(`wiki.abfelbaum.dev`)",
        "traefik.http.routers.wiki-abfelbaum.entrypoints=websecure",
        "traefik.http.routers.wiki-abfelbaum.tls.certresolver=leresolver",
      ]
    }

    task "wikijs" {
      driver = "docker"

      config {
        image = "ghcr.io/requarks/wiki:2.5.292"
      }

      vault {
        env      = false
        policies = ["wiki-abfelbaum"]
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/environment.env"
        env         = true
        data        = <<EOF
  {{ with secret "kv/data/tools/wiki-abfelbaum" }}
            DB_USER={{.Data.data.DB_USER}}
            DB_PASS={{.Data.data.DB_PASSWORD}}
            DB_NAME={{.Data.data.DB_DATABASE}}
  {{ end }}
          EOF
      }

      env {
        DB_TYPE = "postgres"
        DB_HOST = "${NOMAD_UPSTREAM_IP_wikijs_abfelbaum_pgsql}"
        DB_PORT = "${NOMAD_UPSTREAM_PORT_wikijs_abfelbaum_pgsql}"
      }
    }
  }
}