job "hedgedoc" {
  datacenters = ["luna"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  group "database" {
    count = 1

    network {
      mode = "bridge"
    }


    service {
      name = "hedgedoc-pgsql"
      port = 5432

      connect {
        sidecar_service {}
      }
    }

    volume "data" {
      type            = "csi"
      source          = "hedgedoc-pgsql"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    task "postgres" {
      driver = "docker"

      config {
        image = "postgres:13.4-alpine"
      }

      volume_mount {
        volume      = "data"
        destination = "/var/lib/postgresql/data/"
      }

      vault {
        env      = false
        policies = ["hedgedoc"]
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/environment.database.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/tools/hedgedoc" }}
          POSTGRES_USER={{.Data.data.DB_USER}}
          POSTGRES_PASSWORD={{.Data.data.DB_PASSWORD}}
          POSTGRES_DB={{.Data.data.DB_DATABASE}}
{{ end }}
        EOF
      }
    }
  }

  group "hedgedoc" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "hedgedoc"
      port = 3000

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "hedgedoc-pgsql"
              local_bind_port  = 5432
            }
          }
        }
      }

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.hedgedoc.rule=Host(`pad.abfelbaum.dev`)",
        "traefik.http.routers.hedgedoc.entrypoints=websecure",
        "traefik.http.routers.hedgedoc.tls.certresolver=leresolver",
      ]
    }

    volume "uploads" {
      type            = "csi"
      source          = "hedgedoc-uploads"
      read_only       = true
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    task "hedgedoc" {
      driver = "docker"

      config {
        image = "quay.io/hedgedoc/hedgedoc:1.9.9"
      }

      volume_mount {
        volume      = "uploads"
        destination = "/hedgedoc/public/uploads/"
        read_only   = false
      }

      vault {
        env      = false
        policies = ["hedgedoc"]
      }

      template {
        destination = "${NOMAD_SECRETS_DIR}/environment.env"
        env         = true
        data        = <<EOF
  {{ with secret "kv/data/tools/hedgedoc" }}
            CMD_DB_USERNAME={{.Data.data.DB_USER}}
            CMD_DB_PASSWORD={{.Data.data.DB_PASSWORD}}
            CMD_DB_DATABASE={{.Data.data.DB_DATABASE}}

            CMD_SESSION_SECRET={{.Data.data.SESSION_SECRET}}
            CMD_OAUTH2_CLIENT_ID={{.Data.data.OAUTH_CLIENT_ID}}
            CMD_OAUTH2_CLIENT_SECRET={{.Data.data.OAUTH_CLIENT_SECRET}}
  {{ end }}
          EOF
      }

      env {
        TEST = true
        NODE_ENV = "production"

        CMD_DOMAIN = "pad.abfelbaum.dev"

        CMD_PROTOCOL_USESSL = true
        CMD_URL_ADDPORT     = false

        CMD_DB_HOST    = "${NOMAD_UPSTREAM_IP_hedgedoc_pgsql}"
        CMD_DB_PORT    = "${NOMAD_UPSTREAM_PORT_hedgedoc_pgsql}"
        CMD_DB_DIALECT = "postgres"

        CMD_ALLOW_ANONYMOUS                = false
        CMD_ALLOW_ANONYMOUS_EDITS          = true
        CMD_REQUIRE_FREEURL_AUTHENTICATION = true

        CMD_EMAIL = false

        CMD_ALLOW_ORIGIN = "[\"pad.abfelbaum.dev\"]"

        CMD_OAUTH2_USER_PROFILE_USERNAME_ATTR     = "sub"
        CMD_OAUTH2_USER_PROFILE_DISPLAY_NAME_ATTR = "given_name"
        CMD_OAUTH2_USER_PROFILE_EMAIL_ATTR        = "email"
        CMD_OAUTH2_USER_PROFILE_URL               = "https://auth.abfelbaum.dev/oidc/v1/userinfo"
        CMD_OAUTH2_TOKEN_URL                      = "https://auth.abfelbaum.dev/oauth/v2/token"
        CMD_OAUTH2_AUTHORIZATION_URL              = "https://auth.abfelbaum.dev/oauth/v2/authorize"
        CMD_OAUTH2_PROVIDERNAME                   = "AbfiAuth"
        CMD_OAUTH2_SCOPE                          = "openid email profile"
      }
    }
  }
}
