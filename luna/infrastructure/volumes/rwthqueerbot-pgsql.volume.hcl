id = "rwthqueerbot-pgsql" # ID as seen in nomad
name = "rwthqueerbot-pgsql" # Display name
type = "csi"
plugin_id = "nfs" # Needs to match the deployed plugin

capability {
  access_mode     = "multi-node-multi-writer"
  attachment_mode = "file-system"
}