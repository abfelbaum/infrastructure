id = "abfibin-testing-pgsql" # ID as seen in nomad
name = "abfibin-testing-pgsql" # Display name
type = "csi"
plugin_id = "nfs" # Needs to match the deployed plugin

capability {
  access_mode     = "multi-node-multi-writer"
  attachment_mode = "file-system"
}